#!/bin/bash

# Subshell so caller doesn't get dropped into unexpected path
(
# Needs to run from the dir it's in
unset CDPATH
cd "$(dirname -- "$0")" || exit 1

openapi-generator generate -g rust -o spacetraders-sdk -p packageName="spacetraders-sdk" -p packageVersion="2.0.0" -i 'https://stoplight.io/api/v1/projects/spacetraders/spacetraders/nodes/reference/SpaceTraders.json?fromExportButton=true&snapshotType=http_service&deref=optimizedBundle'
)
