# GetStatus200ResponseStats

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agents** | **i32** |  | 
**ships** | **i32** |  | 
**systems** | **i32** |  | 
**waypoints** | **i32** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


