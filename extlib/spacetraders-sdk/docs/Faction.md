# Faction

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**symbol** | **String** |  | 
**name** | **String** |  | 
**description** | **String** |  | 
**headquarters** | **String** |  | 
**traits** | [**Vec<crate::models::FactionTrait>**](FactionTrait.md) |  | 
**is_recruiting** | **bool** | Whether or not the faction is currently recruiting new agents. | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


