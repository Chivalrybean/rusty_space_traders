# GetStatus200ResponseLeaderboards

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**most_credits** | [**Vec<crate::models::GetStatus200ResponseLeaderboardsMostCreditsInner>**](get_status_200_response_leaderboards_mostCredits_inner.md) |  | 
**most_submitted_charts** | [**Vec<crate::models::GetStatus200ResponseLeaderboardsMostSubmittedChartsInner>**](get_status_200_response_leaderboards_mostSubmittedCharts_inner.md) |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


