# GetStatus200ResponseLeaderboardsMostCreditsInner

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**agent_symbol** | **String** |  | 
**credits** | **i32** |  | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


