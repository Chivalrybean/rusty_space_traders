use cursive::views::{Dialog, TextView};

fn main() {
    let mut siv = cursive::default();

    siv.add_layer(Dialog::around(TextView::new("Spaaaaace!"))
                  .title("SPACE!!")
                  .button("You will not go to space today.",
                          |s| s.quit()));

    siv.run();
}
